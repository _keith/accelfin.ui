export default class OperationStatus {
    constructor(
        public name:string,
        public isConnected:boolean
    ) {}
}
