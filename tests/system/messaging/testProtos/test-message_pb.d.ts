export class LoginRequestDto extends __protobuf.Message {
    getUsername():string;
    setUsername(value:string):void;

    getPassword():string;
    setPassword(value:string):void;
}

export class SomeResponseDto extends __protobuf.Message {
    getResponseData():string;
    setResponseData(value:string):void;
}