Pulled from: 
https://github.com/DefinitelyTyped/DefinitelyTyped/tree/cc3d223a946f661eff871787edeb0fcb8f0db156/node-uuid

Note I imported this .d.ts manually as typings doesn't pull down all the bits.
Looks like the issues was mentioned here https://github.com/DefinitelyTyped/DefinitelyTyped/issues/8275
